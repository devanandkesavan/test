@extends('layouts.master')
@section('content')


<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">Show User</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href="{{ route('user.index') }}"> Back</a>
                </div>

                <div class="table-responsive">
					<div class="container">
					
					<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $user->username }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Company Name:</strong>
                {{ $user->companyname }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $user->email }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Phone Number:</strong>
                {{ $user->phone_no }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Role:</strong>
                {{ $user->role }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status:</strong>
                @if ($user->is_active=="1")
                {{"active"}}
                @else
                {{"Inactive"}}
                @endif
              
            </div>
        </div>
    </div>
					
					</div>
                </div>
            </div>
        </div>
	</div>
</div>


@endsection