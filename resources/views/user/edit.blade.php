@extends('layouts.master')
   
@section('content')


<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">Edit User</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href="{{ route('user.index') }}"> Back</a>
                </div>

                <div class="table-responsive">
					<div class="container">
					
					@if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('user.update',$user->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="username" value="{{ $user->username }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Change Password:</strong>
                    <input type="password" name="password" value="{{ $user->password }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Email:</strong>
                    <input type="email" name="email" value="{{ $user->email }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Phone No:</strong>
                    <input type="text" name="phone_no" value="{{ $user->phone_no }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Role:</strong>
                    <input type="text" name="role" value="{{ $user->role }}" class="form-control" placeholder="Name">
                </div>
            </div>
           
            <div class="form-group">
                <input class="mt-2" type="checkbox" name="is_active" value="1" checked="">
                <label class="mt-2"><strong>Active</strong></label>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center btnsubmitpad">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
					
					</div>
                </div>
            </div>
        </div>
	</div>
</div>


@endsection