@extends('layouts.master')
  
@section('content')


<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">Transfer Ware House</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href="{{ route('transferlist.index') }}"> Back</a>
                </div>

                <div class="table-responsive">
					<div class="container">
					
					    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
   
                        <form action="{{ route('transferlist.store') }}" method="POST">
                            @csrf
                        
                            <div class="row">
                                <div class="col">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Ware House (FROM):</strong>
                                            <select name="warehouse" class="form-control">
                                            @foreach ($warehouse as $value)
                                                <option value="{{$value->name}}">{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Ware House (TO):</strong>
                                            <select name="warehouse" class="form-control">
                                            @foreach ($warehouse as $value)
                                                <option value="{{$value->name}}">{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                               </div>
                               <div class="col">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Status:</strong>
                                            <select name="warehouse" class="form-control">
                                                <option>Complete</option>
                                                <option>Pending</option>
                                                <option>Sent</option>
                                            </select>
                                        </div>
                                    </div>
                               </div>
                               <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Select Product:</strong>
                                        <input type="text" name="username" value="" class="form-control" placeholder="Type Product Code">
                                    </div>
                                </div>
                            </div>
                        
                        </form>
					
					</div>
                </div>
            </div>
        </div>
	</div>
</div>


@endsection