@extends('layouts.master')
 
@section('content')


<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Transfer List</h1>
</div>


<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
    <a class="btn btn-success" href="{{ route('transferlist.create') }}"> Create New Transfer</a>
    </div>
   
  

    <div class="table-responsive">
      
      <table ui-jq="dataTable" id="example" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped table-bordered b-t b-b">
      
        <thead>
          <tr>
           
          <th>No</th>
            <th>Date</th>
            <th>Reference No</th>
            <th>Warehouse (From)</th>
            <th>Warehouse (To)</th>
            <th>Product Cost</th>
            <th>Product Tax</th>
            <th>Grand Total</th>
            <th>Status</th>
            <th width="280px">Action</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($transferlist as $transferlist)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $transferlist->date }}</td>
            <td>{{ $transferlist->reference_no }}</td>
            <td>{{ $transferlist->warehouse_from }}</td>
            <td>{{ $transferlist->warehouse_to }}</td>
            <td>{{ $transferlist->product_cost }}</td>
            <td>{{ $transferlist->product_tax }}</td>
            <td>{{ $transferlist->grand_total }}</td>
            <td>{{ $transferlist->status }}</td>
            <td>
                <form action="{{ route('transferlist.destroy', $transferlist->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('transferlist.show',$transferlist->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('transferlist.edit',$transferlist->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
            


                            
        </tbody>
      </table>
    </div>
  </div>
</div>



	</div>
  </div>
  <!-- /content -->

      
@endsection