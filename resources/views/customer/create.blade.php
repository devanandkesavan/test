@extends('layouts.master')
 
@section('content')
<!-- content -->
<div id="content" class="app-content" role="main">
  <div class="app-content-body "> 
    <div class="bg-light lter b-b wrapper-md">
      <h1 class="m-n font-thin h3">Add New Customer</h1>
    </div>

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="wrapper-md">
      <div class="panel panel-default">
      <div class="panel-heading btnbck">
            <a class="btn btn-success" href="{{ route('customer.index') }}"> Back</a>
          </div>
        <div class="table-responsive">
          <div class="container">
          <form action="{{ route('customer.store') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>Customer Group *</strong>
                <select name="customer_group" class="form-control">
                <option value="General">General</option>
                <option value="Distributer">Distributer</option>
                <option value="Reseller">Reseller</option>
                </select>
            </div>
        </div>
        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>Customer Name</strong>
                <input type="text" name="name" class="form-control" placeholder="Customer Name">
            </div>
        </div>

        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>Company Name</strong>
                <input type="text" name="company_name" class="form-control" placeholder="Company Name">
            </div>
        </div>
        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>Email</strong>
                <input type="email" name="email" class="form-control" placeholder="Enter Your Email">
                
                
            </div>
        </div>
        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>Phone Number *</strong>
                <input type="text" name="phone_number" class="form-control" placeholder="Enter Your Phone Number">
                
               
            </div>
        </div>
        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>Tax Number</strong>
                <input type="text" name="tax_no" class="form-control" placeholder="Enter Your tax Number">
                
               
            </div>
        </div>
        
        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>Address *</strong>
                <input type="text" name="address" class="form-control" placeholder="Enter Your Address">
                
               
            </div>
        </div>
        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>City *</strong>
                <input type="text" name="city" class="form-control" placeholder="Enter Your City">
                
               
            </div>
        </div>
        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>State</strong>
                <input type="text" name="state" class="form-control" placeholder="Enter Your State">
                
               
            </div>
        </div>
        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>Country</strong>
                <input type="text" name="country" class="form-control" placeholder="Enter Your Country">
                
               
            </div>
        </div>
        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>ZipCode</strong>
                <input type="text" name="postal_code" class="form-control" placeholder="Enter Your Zipcode">
                
               
            </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>User:</strong>
                <select name="user_id" class="form-control">
                @foreach ($user as $val)
                    <option value="{{$val->id}}">{{$val->username}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-xs-10 col-sm-10 col-md-6">
            <div class="form-group">
                <strong>description:</strong>
                <textarea class="form-control" style="height:150px" name="description" placeholder="description"></textarea>
            </div>
        </div>
        <div class="col-xs-10 col-sm-10 col-md-6 text-center btnsub">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
            </div>
          </div>
        </div>
          
      </div>
  </div>
</div>
</div>
</div>
@endsection










































