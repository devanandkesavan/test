@extends('layouts.master')
 
@section('content')
<!-- content -->
<div id="content" class="app-content" role="main">
  <div class="app-content-body ">
    <div class="bg-light lter b-b wrapper-md">
      <h1 class="m-n font-thin h3">Customer List</h1>
    </div>
    <div class="wrapper-md">
      <div class="panel panel-default">
        <div class="panel-heading">
          <a class="btn btn-success" href="{{ route('customer.create') }}"> Create New Customer</a>
        </div>
      <div class="table-responsive">
        <table ui-jq="dataTable" id="example" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped table-bordered b-t b-b">
          <thead>
            <tr>
            <th>No</th>
            <th>Customer Group</th>
            <th>Name</th>
            <th>Company Name</th>
            <th>Email</th>
            <th>Phone No</th>
            <th width="280px">Action</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($customer as $value)
              <tr>
              <td>{{ ++$i }}</td>
            <td>{{ $value->customer_group }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->company_name }}</td>
            <td>{{ $value->email }}</td>
            <td>{{ $value->phone_number }}</td>
           
            <td>
                <form action="{{ route('customer.destroy', $value->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('customer.show',$value->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('customer.edit',$value->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
              </tr>
            @endforeach                  
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
  <!-- /content -->

      
@endsection









































