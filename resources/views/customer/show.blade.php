@extends('layouts.master')
@section('content')

<!-- content -->
<div id="content" class="app-content" role="main">
  <div class="app-content-body ">
    <div class="bg-light lter b-b wrapper-md">
      <h1 class="m-n font-thin h3">Show Customer</h1>
    </div>
    <div class="wrapper-md">
      <div class="panel panel-default">
         <div class="table-responsive">
          <div class="container">
          <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <i class='fas fa-user-friends'></i>
                <strong>Customer Group:</strong>
                {{ $customer->customer_group }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <i class='fas fa-user'></i>
                <strong>Customer Name:</strong>
                {{ $customer->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <i class='far fa-building'></i>
                <strong>Company Name:</strong>
                    {{ $customer->company_name }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                    <i class='fas fa-envelope'></i>
                        <strong>Email:</strong>
                        {{ $customer->email }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                        
                        <i class='fas fa-phone'></i>
                        <strong>Phone Number:</strong>
                            {{ $customer->phone_number }}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                        <i class='fas fa-print'></i>
                            <strong>Tax Number:</strong>
                            {{ $customer->tax_no }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                        <i class='fas fa-address-card'></i>
                            <strong>Address:</strong>
                            {{ $customer->address }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                        <i class='fas fa-city'></i>
                            <strong>City:</strong>
                            {{ $customer->city }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                        <i class='fas fa-city'></i>
                            <strong>state:</strong>
                            {{ $customer->state }}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                        <i class='far fa-file'></i>
                            <strong>Zipcode:</strong>
                            {{ $customer->postal_code }}
                        </div>
                    </div>

                    
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                        <i class='fas fa-globe'></i>
                            <strong>Country:</strong>
                            {{ $customer->country }}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                            <i class='fas fa-file-alt'></i>
                                <strong>Description:</strong>
                                {{ $customer->description }}
                            </div>
                        </div>
    </div>
      @if ($errors->any())
        <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
          </ul>
    <div>
@endif
            


                            
        
    </div>
  </div>
</div>



	</div>
  </div>
  <!-- /content -->

      
@endsection































