@extends('layouts.master')
  
@section('content')

<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">Add Product</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href="{{ route('product.index') }}"> Back</a>
                </div>

                <div class="table-responsive">
					<div class="container"> 

                    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('product.store') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Name:</strong>
                <input type="text" name="product_name" class="form-control" placeholder="Product Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Code:</strong>
                <input type="text" name="product_code" class="form-control" placeholder="Product Code">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Category:</strong>
                <select name="category" class="form-control">
                        <option value="">--Select--</option>
                    @foreach ($category as $item)
                        <option value="{{$item->name}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Unit:</strong>
                <select name="unit" class="form-control">
                        <option value="">--Select--</option>
                        @foreach ($unit as $item)
                            <option value="{{$item->name}}">{{$item->name}}</option>
                        @endforeach
                </select>
                
                
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Bin:</strong>
                <select name="bin" class="form-control">
                        <option value="">--Select--</option>
                        @foreach ($bin as $item)
<option value="{{$item->name}}">{{$item->name}}</option>
@endforeach
                </select>
                
               
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description:</strong>
                <textarea class="form-control" style="height:150px" name="description" placeholder="Description"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center btnsubmitpad">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form> 

					</div>
                </div>
            </div>
        </div>
	</div>
</div>


@endsection