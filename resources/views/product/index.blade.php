@extends('layouts.master')
 
@section('content')

<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">List Product</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a class="btn btn-success" href="{{ route('product.create') }}"> Add Product</a>
                </div>
                @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif
                <div class="table-responsive">
                <table ui-jq="dataTable" id="example" ui-options="{
                    sAjaxSource: 'api/datatable.json',
                    aoColumns: [
                        { mData: 'engine' },
                        { mData: 'browser' },
                        { mData: 'platform' },
                        { mData: 'version' },
                        { mData: 'grade' }
                    ]
                    }" class="table table-striped table-bordered b-t b-b">

                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
    
                        <div class="container">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Product Name</th>
                                    <th>Product code</th>
                                    <th>Category</th>
                                    <th>Bin</th>
                                    <th>Unit</th>
                                    <th>Description</th>
                                    <th width="280px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($product as $value)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $value->product_name }}</td>
                                    <td>{{ $value->product_code }}</td>
                                    <td>{{ $value->category }}</td>
                                    <td>{{ $value->unit }}</td>
                                    <td>{{ $value->bin }}</td>
                                    <td>{{ $value->description }}</td>
                                    <td>
                                        <form action="{{ route('product.destroy', $value->id) }}" method="POST">
                        
                                            <a class="btn btn-info" href="{{ route('product.show',$value->id) }}">Show</a>
                            
                                            <a class="btn btn-primary" href="{{ route('product.edit',$value->id) }}">Edit</a>
                        
                                            @csrf
                                            @method('DELETE')
                            
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            <tbody>
                        </div>
                      

                </table>
                </div>
            </div>
        </div>
	</div>
</div>


@endsection