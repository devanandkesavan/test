@extends('layouts.master')
@section('content')

<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">Show Product</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href="{{ route('product.index') }}"> Back</a>
                </div>

                <div class="table-responsive">
					<div class="container">

                    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Name:</strong>
                {{ $product->product_name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Code:</strong>
                {{ $product->product_code }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Unit:</strong>
                    {{ $product->unit }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Bin:</strong>
                        {{ $product->bin }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Category:</strong>
                            {{ $product->category }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Description:</strong>
                                {{ $product->description }}
                            </div>
                        </div>
    </div>

					</div>
                </div>
            </div>
        </div>
	</div>
</div>

@endsection