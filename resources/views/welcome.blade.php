<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
        <link rel="stylesheet" href="{{asset('css/simple-line-icons.css')}}">
        <link rel="stylesheet" href="{{asset('css/font.css')}}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/mse.css')}}">
        <script src="{{asset('js/jquery.js')}}"></script>
        <script src="{{asset('js/app.js')}}"></script>
        <script src="{{asset('js/bootstrap.js')}}"></script>
        <script src='{{asset('js/jquery.dataTables.min.js')}}'></script>
        <link rel="stylesheet" href="{{asset('css/jquery.dataTables.min.css')}}" type="text/css" />
        <link rel="stylesheet" href="{{asset('css/buttons.dataTables.min.css')}}" type="text/css" />
</head>
<body>
       
<div class="container">
    <div class="row">
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4 lgnset">
                <div class="from-group tal">
                    <center><label for="name">SOCOMEC</label></center>
                </div>
                    <form action="#" method="post">
                        <div class="form-group">
                            <label for="username">User Name:</label>
                            <input type="username" class="form-control" id="username">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="pwd">
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
            </div>
            <div class="col-sm-4">
                
            </div>
    </div>
</div>

 
    <script src="{{asset('js/ui-client.js')}}"></script>
    <script src="{{asset('js/ui-jp.config.js')}}"></script>
    <script src="{{asset('js/ui-jp.js')}}"></script>
    <script src="{{asset('js/ui-load.js')}}"></script>
    <script src="{{asset('js/ui-nav.js')}}"></script>
    <script src="{{asset('js/ui-toggle.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print'
        ]
    } );
} );

</script>
    @yield('scripts')
</body>
</html>




