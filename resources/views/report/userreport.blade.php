@extends('layouts.master')
  
@section('content')

<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">USER REPORT</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href=""> Back</a>
                </div>

                <div class="table-responsive">
					<div class="container">
                        <div class="row">
                    
                            <div class="col-xs-12 col-sm-12 col-md-5">
                            

                                <div class="form-group dpr center">

                                

                                    <div class="input-daterange input-group" id="datepicker" name="datepicker">
                                        <span class="input-group-addon">From</span>
                                        <input type="text" class="input-sm form-control" name="start_date" />
                                        <span class="input-group-addon">To</span>
                                        <input type="text" class="input-sm form-control" name="end_date" />
                                    </div>

                                
                            
                                </div>
                            
                            </div>

                            <div class="col-xs-5 col-sm-5 col-md-5">
                                <div class="from-group dprsp">
                                <button class="btn btn-Basic dropdown-toggle" type="button" data-toggle="dropdown">SELECT THE WAREHOUSE
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                    @foreach ($warehouse as $val)
                                        <li><a href="" value="{{$val->id}}">{{$val->warehousename}}</a></li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>          
                    </div>
                    
                    <table ui-jq="dataTable" id="example" ui-options="{
                            sAjaxSource: 'api/datatable.json',
                            aoColumns: [
                                { mData: 'engine' },
                                { mData: 'browser' },
                                { mData: 'platform' },
                                { mData: 'version' },
                                { mData: 'grade' }
                            ]
                            }" class="table table-striped table-bordered b-t b-b">
                        
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Contact No</th>
                                <th>Company Name</th>
                                <th>Role</th>
                                <th>Warehouse</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($userreport as $usrrpt)
                            <tr>
                                <td>NULL</td>
                                <td>{{$usrrpt->username }}</td>
                                <td>{{$usrrpt->email}}</td>
                                <td>{{$usrrpt->phone_no}}
                                <td>{{$usrrpt->companyname}}</td>
                                <td>{{$usrrpt->role}}</td>
                                <td>{{$usrrpt->warehouse}}</td>
                                <td>{{$usrrpt->created_at}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                    </table> 
					</div>
                </div>
            </div>
        </div>
       
        
		
        </div>
	</div>
</div>

<script>
    $('.input-daterange').datepicker({
        format: 'yyyy-mm-dd'
    });
</script>
@endsection