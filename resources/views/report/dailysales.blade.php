@extends('layouts.master')
  
@section('content')

<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">DAILY SALES REPORT</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href=""> Back</a>
                </div>

                <div class="table-responsive">
					<div class="container">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group dpr center">
                                <strong>Daily Purchase Report:</strong>
                                    
                                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">SELECT WAREHOUSE
                                        <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">DEMO WAREHOUSE</a></li>
                                        </ul>

                                        <table ui-jq="dataTable" id="example" ui-options="{
                                                sAjaxSource: 'api/datatable.json',
                                                aoColumns: [
                                                    { mData: 'engine' },
                                                    { mData: 'browser' },
                                                    { mData: 'platform' },
                                                    { mData: 'version' },
                                                    { mData: 'grade' }
                                                ]
                                                }" class="table table-striped table-bordered b-t b-b">
                                            
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone No</th>
                                                    <th>Address</th>
                                                    <th width="280px">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($warehouse as $value)
                                                <tr>
                                                    <td>{{ ++$i }}</td>
                                                    <td>{{ $value->name }}</td>
                                                    <td>{{ $value->email }}</td>
                                                    <td>{{ $value->phone_no }}</td>
                                                    <td>{{ $value->address }}</td>
                                                
                                                    <td>
                                                        <form action="{{ route('warehouse.destroy', $value->id) }}" method="POST">
                                        
                                                            <a class="btn btn-info" href="{{ route('warehouse.show',$value->id) }}">Show</a>
                                            
                                                            <a class="btn btn-primary" href="{{ route('warehouse.edit',$value->id) }}">Edit</a>
                                        
                                                            @csrf
                                                            @method('DELETE')
                                            
                                                            <button type="submit" class="btn btn-danger">Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>

                                    
                                </div>
                            </div>
                        </div>
					</div>
                </div>
            </div>
        </div>
       
   
		
        </div>
	</div>
</div>
@endsection