@extends('layouts.master')
  
@section('content')

<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">Add Ware House</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href="{{ route('warehouse.index') }}"> Back</a>
                </div>

                <div class="table-responsive">
					<div class="container">
					
					@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('warehouse.store') }}" method="POST">
    @csrf
    <div class="row">
      
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Name *</strong>
                <input type="text" name="name" class="form-control" placeholder="Warehouse Name">
            </div>
        </div>
       
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Email</strong>
                <input type="email" name="email" class="form-control" placeholder="Enter Your Email">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Phone Number *</strong>
                <input type="text" name="phone_no" class="form-control" placeholder="Enter Your Phone Number">
            </div>
        </div>
       
        
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Address *</strong>
                <input type="text" name="address" class="form-control" placeholder="Enter Your Address">
            
            </div>
        </div>
       

      
        <div class="col-xs-12 col-sm-12 col-md-6 text-center btnsubmitpad">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
					
					</div>
                </div>
            </div>
        </div>
	</div>
</div>


@endsection