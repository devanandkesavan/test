@extends('layouts.master')
@section('content')

<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">Show Ware House</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href="{{ route('warehouse.index') }}"> Back</a>
                </div>

                <div class="table-responsive">
					<div class="container">
					
					<div class="row">
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Warehouse Name:</strong>
                {{ $warehouse->warehousename }}
            </div>
        </div>
        
            <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Email:</strong>
                        {{ $warehouse->email }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Phone Number:</strong>
                            {{ $warehouse->phone_no }}
                        </div>
                    </div>

                    
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Address:</strong>
                            {{ $warehouse->address }}
                        </div>
                    </div>
                   
    </div>
					
					</div>
                </div>
            </div>
        </div>
	</div>
</div>


@endsection