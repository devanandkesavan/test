@extends('layouts.master')
  
@section('content')

<!-- content -->
<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">Add Bin</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href="{{ route('bins.index') }}"> Back</a>
                </div>

                <div class="table-responsive">

                    <form action="{{ route('bins.store') }}" method="POST">
                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        <input type="text" name="name" class="form-control" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Detail:</strong>
                                        <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail"></textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center btnsubmitpad">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
   
                    </form>

                </div>
            </div>
        </div>
	</div>
</div>


@endsection