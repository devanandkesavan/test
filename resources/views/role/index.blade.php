@extends('layouts.master')
 
@section('content')


<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Role List</h1>
</div>


<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
    <a class="btn btn-success" href="{{ route('role.create') }}"> Create Role</a>
    </div>
   
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

    <div class="table-responsive">
      
      <table ui-jq="dataTable" id="example" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped table-bordered b-t b-b">
      
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Details</th>
            <th width="280px">Action</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($roles as $role)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $role->name }}</td>
            <td>{{ $role->description }}</td>
            <td>
                <form action="{{ route('role.destroy', $role->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('role.show',$role->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('role.edit',$role->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
            


                            
        </tbody>
      </table>
    </div>
  </div>
</div>



	</div>
  </div>
  <!-- /content -->

      
@endsection