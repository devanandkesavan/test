@extends('layouts.master')
@section('content')


<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">Show Role</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href="{{ route('role.index') }}"> Back</a>
                </div>

                <div class="table-responsive">
					<div class="container">
					
					<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $role->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Details:</strong>
                {{ $role->description }}
            </div>
        </div>
    </div>
					
					</div>
                </div>
            </div>
        </div>
	</div>
</div>


@endsection