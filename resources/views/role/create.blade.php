@extends('layouts.master')
  
@section('content')

<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h3">Add Role</h1>
        </div>

        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading btnbck">
                    <a class="btn btn-success" href="{{ route('role.index') }}"> Back</a>
                </div>

                <div class="table-responsive">
					<div class="container">
					
					@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('role.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Detail:</strong>
                <textarea class="form-control" style="height:150px" name="description" placeholder="Detail"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center btnsubmitpad">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
					
					</div>
                </div>
            </div>
        </div>
	</div>
</div>


@endsection