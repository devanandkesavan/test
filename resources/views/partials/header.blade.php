<header id="header" class="app-header navbar" role="menu">
        <!-- navbar header -->
        <div class="navbar-header bg-dark">
          <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
            <i class="glyphicon glyphicon-cog"></i>
          </button>
          <button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
            <i class="glyphicon glyphicon-align-justify"></i>
          </button>
          <!-- brand -->
          <a href="#/" class="navbar-brand text-lt">
            {{-- <i class="fa fa-btc"></i> --}}
            {{-- <img src="{{asset('images/logo.png')}}" alt="." class="hide"> --}}
            <img src="{{asset('images/logo.png')}}" alt=".">
            <span class="hidden-folded m-l-xs">Socomec</span>
          </a>
          <!-- / brand -->
        </div>
        <!-- / navbar header -->
  
        <!-- navbar collapse -->
        <div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
          <!-- buttons -->
          <div class="nav navbar-nav hidden-xs">
            <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="app-aside-folded" target=".app">
              <i class="fa fa-dedent fa-fw text"></i>
              <i class="fa fa-indent fa-fw text-active"></i>
            </a>
          
          </div>
          <!-- / buttons -->
  
         
  
          <!-- nabar right -->
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                    <i class="fa fa-bell-o"></i>
                <span class="visible-xs-inline">Notifications</span>
                <span class="badge badge-sm up bg-danger pull-right-xs">2</span>
              </a>
              <!-- dropdown -->
              <div class="dropdown-menu w-xl animated fadeInUp">
                <div class="panel bg-white">
                  <div class="panel-heading b-light bg-light">
                    <strong>You have <span>2</span> notifications</strong>
                  </div>
                  <div class="list-group">
                    <a href class="list-group-item">
                      <span class="pull-left m-r thumb-sm">
                        <img src="{{asset('images/a0.jpg')}}" alt="..." class="img-circle">
                      </span>
                      <span class="clear block m-b-none">
                        Use awesome animate.css<br>
                        <small class="text-muted">10 minutes ago</small>
                      </span>
                    </a>
                    <a href class="list-group-item">
                      <span class="clear block m-b-none">
                        1.0 initial released<br>
                        <small class="text-muted">1 hour ago</small>
                      </span>
                    </a>
                  </div>
                  <div class="panel-footer text-sm">
                    <a href class="pull-right"><i class="fa fa-cog"></i></a>
                    <a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                  </div>
                </div>
              </div>
              <!-- / dropdown -->
            </li>
            <li class="dropdown">
              <a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
                <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                  <img src="{{asset('images/a0.jpg')}}" alt="...">
                  <i class="on md b-white bottom"></i>
                </span>
                <span class="hidden-sm hidden-md">John.Smith</span> <b class="caret"></b>
              </a>
              <!-- dropdown -->
              <ul class="dropdown-menu animated fadeInRight w">
                <li class="wrapper b-b m-b-sm bg-light m-t-n-xs">
                  <div>
                    <p>300mb of 500mb used</p>
                  </div>
                  <div class="progress progress-xs m-b-none dker">
                    <div class="progress-bar progress-bar-info" data-toggle="tooltip" data-original-title="50%" style="width: 50%"></div>
                  </div>
                </li>
                <li>
                  <a href>
                    <span class="badge bg-danger pull-right">30%</span>
                    <span>Settings</span>
                  </a>
                </li>
                <li>
                  <a ui-sref="app.page.profile">Profile</a>
                </li>
                <li>
                  <a ui-sref="app.docs">
                    <span class="label bg-info pull-right">new</span>
                    Help
                  </a>
                </li>
                <li class="divider"></li>
                <li>
                  <a ui-sref="access.signin">Logout</a>
                </li>
              </ul>
              <!-- / dropdown -->
            </li>
          </ul>
          <!-- / navbar right -->
        </div>
        <!-- / navbar collapse -->
    </header>
    <!-- / header -->
  
  
      <!-- aside -->
    <aside id="aside" class="app-aside hidden-xs bg-dark">
        <div class="aside-wrap">
          <div class="navi-wrap">
            <!-- user -->
            <div class="clearfix hidden-xs text-center hide" id="aside-user">
              <div class="dropdown wrapper">
                <a href="app.page.profile">
                  <span class="thumb-lg w-auto-folded avatar m-t-sm">
                    <img src="{{asset('images/a0.jpg')}}" class="img-full" alt="...">
                  </span>
                </a>
                <a href="#" data-toggle="dropdown" class="dropdown-toggle hidden-folded">
                  <span class="clear">
                    <span class="block m-t-sm">
                      <strong class="font-bold text-lt">John.Smith</strong> 
                      <b class="caret"></b>
                    </span>
                    <span class="text-muted text-xs block">Art Director</span>
                  </span>
                </a>
                <!-- dropdown -->
                <ul class="dropdown-menu animated fadeInRight w hidden-folded">
                 
                  <li>
                    <a href>Settings</a>
                  </li>
                  <li>
                    <a href="page_profile.html">Profile</a>
                  </li>
                 
                  <li class="divider"></li>
                  <li>
                    <a href="page_signin.html">Logout</a>
                  </li>
                </ul>
                <!-- / dropdown -->
              </div>
              <div class="line dk hidden-folded"></div>
            </div>
            <!-- / user -->
  
            <!-- nav -->
            <li class="line"></li>
            <nav ui-nav class="navi clearfix">
              <ul class="nav">
                <li>
                  <a href="/">
                    <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
                    <span>Dashboard</span>
                  </a>
                </li>
                <li>
                  <a href class="auto">      
                    <span class="pull-right text-muted">
                      <i class="fa fa-fw fa-angle-right text"></i>
                      <i class="fa fa-fw fa-angle-down text-active"></i>
                    </span>
                    {{-- <i class="glyphicon glyphicon-stats icon text-primary-dker"></i> --}}
                    <i class="fa fa-sitemap text-primary-dker" aria-hidden="true"></i>
                    <span class="font-bold">Products</span>
                  </a>
                  <ul class="nav nav-sub dk">
                   
                    <li>
                      <a href="Productlist">
                        <span>Product List</span>
                      </a>
                    </li>
                    
                    <li>
                      <a href="Categorylist">
                        <span>Category List</span>
                      </a>
                    </li>
                   
                    <li>
                      <a href="Stockcount">
                       
                        <span>Stock Count</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href class="auto">      
                    <span class="pull-right text-muted">
                      <i class="fa fa-fw fa-angle-right text"></i>
                      <i class="fa fa-fw fa-angle-down text-active"></i>
                    </span>
                    <i class="fa fa-exchange text-primary-dker" aria-hidden="true"></i>
                    <span class="font-bold">Transfer</span>
                  </a>
                  <ul class="nav nav-sub dk">
                   
                    <li>
                      <a href="Transferlist">
                        <span>Transfer List</span>
                      </a>
                    </li>
                    <li>
                      <a href="Addtransfer">
                        <span>Add a Transfer</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href class="auto">      
                    <span class="pull-right text-muted">
                      <i class="fa fa-fw fa-angle-right text"></i>
                      <i class="fa fa-fw fa-angle-down text-active"></i>
                    </span>
                    <i class="fa fa-cogs text-primary-dker" aria-hidden="true"></i>
                    
                    <span class="font-bold">Settings</span>
                  </a>
                  <ul class="nav nav-sub dk">
                   
                    <li>
                      <a href="Barcode">
                        <span>Bar Code</span>
                      </a>
                    </li>
                    <li>
                      <a href="ViewBins">
                        <span>List Bins</span>
                      </a>
                    </li>
                   
                    <li>
                      <a href="Unitlist">
                        <span>List Unit</span>
                      </a>
                    </li>
                   
                      <li>
                        <a href="Rolelist">
                          <span>List Role</span>
                        </a>
                      </li>
                    
                     
                      <li>
                        <a href="Userlist">
                          <span>List User</span>
                        </a>
                      </li>
                      <li>
                          <a href="Taxlist">
                            <span>List Tax</span>
                          </a>
                        </li>
                    
                    
                    <li>
                      <a href="Adjustment">
                      
                        <span>Adjustments</span>
                      </a>
                    </li>
                    <li>
                      <a href="Users">
                      
                        <span>Users & Roles</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href class="auto">      
                    <span class="pull-right text-muted">
                      <i class="fa fa-fw fa-angle-right text"></i>
                      <i class="fa fa-fw fa-angle-down text-active"></i>
                    </span>
                    <i class="fa fa-file text-primary-dker" aria-hidden="true"></i>
                    
                    <span class="font-bold">Reports</span>
                  </a>
                  <ul class="nav nav-sub dk">
                   
                    <li>
                      <a href="">
                        <span>Sample Report 1</span>
                      </a>
                    </li>
                    <li>
                      <a href="">
                        <span>Sample Report 2</span>
                      </a>
                    </li>
                   
                  </ul>
                </li>
               
              
  
               
               
              </ul>
            </nav>
            <!-- nav -->
  
            {{-- <!-- aside footer -->
           <div class="wrapper m-t">
              <div class="text-center-folded">
                <span class="pull-right pull-none-folded">60%</span>
                <span class="hidden-folded">Milestone</span>
              </div>
              <div class="progress progress-xxs m-t-sm dk">
                <div class="progress-bar progress-bar-info" style="width: 60%;">
                </div>
              </div>
              <div class="text-center-folded">
                <span class="pull-right pull-none-folded">35%</span>
                <span class="hidden-folded">Release</span>
              </div>
              <div class="progress progress-xxs m-t-sm dk">
                <div class="progress-bar progress-bar-primary" style="width: 35%;">
                </div>
              </div>
            </div>
            <!-- / aside footer -->
          </div>
        </div> --}}
        <li class="line dk"></li>
    </aside>
    <!-- / aside -->

    