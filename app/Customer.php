<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'customer_group', 'name','company_name','email','phone_number','tax_no','address','city','state','postal_code','country','description','user_id',
    ];
}
