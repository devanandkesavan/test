<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table="products";
    protected $fillable=['product_name','product_code','category','unit','bin','description'];
}
