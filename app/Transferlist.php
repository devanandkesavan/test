<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transferlist extends Model
{
    public $table="transferlist";
    protected $fillable=['date','reference_no','warehouse_from','warehouse_to','product_cost','product_tax','grand_total','status'];
}
