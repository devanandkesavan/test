<?php

namespace App\Http\Controllers;

use App\Transferlist;
use App\Warehouse;
use Illuminate\Http\Request;

class TransferlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transferlist=Transferlist::latest()->paginate(5);
        return view('transferlist.index',compact('transferlist'))
                                ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warehouse = Warehouse::all();
        return view('transferlist.create',compact('warehouse'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'=>'requried',
            'reference_no'=>'requried',
            'warehouse_from'=>'requried',
            'warehouse_to'=>'requried',
            'product_cost'=>'requried',
            'grand_total'=>'requried',
            'status'=>'requried',

        ]);

        Transferlist::create($request->all());

        return redirect()->route('transferlist.index')
                                        ->with('success', 'Tranferlist Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transferlist  $transferlist
     * @return \Illuminate\Http\Response
     */
    public function show(Transferlist $transferlist)
    {
        return view('transferlist.show',compact('transferlist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transferlist  $transferlist
     * @return \Illuminate\Http\Response
     */
    public function edit(Transferlist $transferlist)
    {
        return view('transferlist.edit',compact('transferlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transferlist  $transferlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transferlist $transferlist)
    {
        $request->validate([
            'date'=>'requried',
            'reference_no'=>'requried',
            'warehouse_from'=>'requried',
            'warehouse_to'=>'requried',
            'product_cost'=>'requried',
            'grand_total'=>'requried',
            'status'=>'requried',

        ]);

        Transferlist::update($request->all());

        return redirect()->route('transferlist.index')
                                        ->with('success', 'Tranferlist Created Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transferlist  $transferlist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transferlist $transferlist)
    {
        $transferlist->delete();

        return redirect()->route('transferlist.index')
                        ->with('success','Transferlist Updated Successfully');
    }
}
