<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Warehouse;
use App\User;
use App\Customer;

class UserrportController extends Controller
{
    public function index()
    {
  return view('report.userreport');
    }

    public function show()
    {
        $user=User::all();
        $warehouse=Warehouse::all();
        $userreport=DB::table('user')
        ->where('warehouse','2')
        ->whereBetween('created_at',array('2019-06-05','2019-06-06'))
        ->get();
        //echo"<pre>";
        //print_r($userreport);
        return view('report.userreport',compact('user','warehouse'))->with('userreport',$userreport);
    }
}