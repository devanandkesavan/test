<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model

{
    public $table="user";
        protected $fillable=['username','password','companyname','email','phone_no','role','warehouse','is_active'];
}
