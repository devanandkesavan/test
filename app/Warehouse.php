<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    public $table="warehouse";
    protected $fillable=['warehousename','email','phone_no','address'];
}
