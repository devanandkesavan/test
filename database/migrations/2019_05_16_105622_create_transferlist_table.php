<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferlistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Transferlist', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date');
            $table->string('reference_no');
            $table->string('warehouse_from');
            $table->string('warehouse_to');
            $table->string('product_cost');
            $table->string('product_tax');
            $table->string('grand_total');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Transferlist');
    }
}
