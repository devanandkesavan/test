<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('bins','BinController');
Route::resource('category','CategoryController');
Route::resource('unit','UnitController');
Route::resource('product','ProductController');
Route::resource('customer','CustomerController');
Route::resource('warehouse','WarehouseController');
Route::resource('role','RoleController');
Route::resource('user','UsersController');
Route::resource('report','ReportController');
Route::resource('report','UserrportController');


Route::get('dashboard', function () {
        return view('/dashboard');
    });

    Route::get('userreport', function () {
        return view('/userreport');
    });